
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class Share {
  static const MethodChannel _channel =
      const MethodChannel('share');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String?> get smsShare async {
    final String? version = await _channel.invokeMethod('smsShare');
    return version;
  }

  static Future<String?> get facebookStoryShare async {
    final String? version = await _channel.invokeMethod('facebookStoryShare');
    return version;
  }

  static Future<String?> instagramStoryShare(String imagePath,
    String? backgroundTopColor,
    String? backgroundBottomColor,
    String? attributionURL,
    String? backgroundImagePath,
  ) async {
    Map<String, dynamic> args;
    final tempDir = await getTemporaryDirectory();

    File file = File(imagePath);
    // ByteData bytes = await rootBundle.load(imagePath);
    Uint8List bytes = file.readAsBytesSync();
    var stickerData = bytes.buffer.asUint8List();
    String stickerAssetName = 'stickerAsset.png';
    final Uint8List stickerAssetAsList = stickerData;
    final stickerAssetPath = '${tempDir.path}/$stickerAssetName';
    file = await File(stickerAssetPath).create();
    file.writeAsBytesSync(stickerAssetAsList);

    String? backgroundAssetName;
    if (backgroundImagePath != null) {
      File backgroundImage = File(backgroundImagePath);
      Uint8List backgroundImageData = backgroundImage.readAsBytesSync();
      backgroundAssetName = 'backgroundAsset.jpg';
      final Uint8List backgroundAssetAsList = backgroundImageData;
      final backgroundAssetPath = '${tempDir.path}/$backgroundAssetName';
      File backFile = await File(backgroundAssetPath).create();
      backFile.writeAsBytesSync(backgroundAssetAsList);
    }

    args = <String, dynamic>{
      "stickerImage": stickerAssetName,
      "backgroundImage": backgroundAssetName,
      "backgroundTopColor": backgroundTopColor,
      "backgroundBottomColor": backgroundBottomColor,
      "attributionURL": attributionURL,
    };
    final String? version = await _channel.invokeMethod('instagramStoryShare', args);
    return version;
  }

  static Future<String?> facebookShareLink(String link) async {
    Map<String, dynamic> args = <String, dynamic>{
      "link": link
    };
    final String? version = await _channel.invokeMethod('facebookShareLink', args);
    return version;
  }

  static Future<String?> facebookShareQuote(String text, String link) async {
    Map<String, dynamic> args = <String, dynamic>{
      "text": text,
      "link": link
    };
    final String? version = await _channel.invokeMethod('facebookShareQuote', args);
    return version;
  }

  static Future<String?> facebookShareImage(String text, String path) async {
    File file = File(path);
    Uint8List bytes = file.readAsBytesSync();
    var stickerdata = bytes.buffer.asUint8List();
    final tempDir = await getTemporaryDirectory();
    String stickerAssetName = 'stickerAsset.png';
    final Uint8List stickerAssetAsList = stickerdata;
    final stickerAssetPath = '${tempDir.path}/$stickerAssetName';
    file = await File(stickerAssetPath).create();
    file.writeAsBytesSync(stickerAssetAsList);

    Map<String, dynamic> args = <String, dynamic>{
      "text": text,
      "path": path,
      "writePath": stickerAssetPath,
    };
    final String? version = await _channel.invokeMethod('facebookShareImage', args);
    return version;
  }

  static Future<String?> instagramShareImage(String text, String path) async {
    File file = File(path);
    Uint8List bytes = file.readAsBytesSync();
    var stickerdata = bytes.buffer.asUint8List();
    final tempDir = await getTemporaryDirectory();
    String stickerAssetName = 'stickerAsset.png';
    final Uint8List stickerAssetAsList = stickerdata;
    final stickerAssetPath = '${tempDir.path}/$stickerAssetName';
    file = await File(stickerAssetPath).create();
    file.writeAsBytesSync(stickerAssetAsList);

    Map<String, dynamic> args = <String, dynamic>{
      "text": text,
      "path": path,
      "writePath": stickerAssetPath,
    };
    final String? version = await _channel.invokeMethod('instagramShareImage', args);
    return version;
  }

  static Future<String?> twitterShare(String captionText,
      List<String>? hashtags, String? url, String? trailingText) async {
    Map<String, dynamic> args;
    String modifiedUrl;
    if (Platform.isAndroid) {
      modifiedUrl = Uri.parse(url!).toString().replaceAll('#', " ");
    } else {
      modifiedUrl = Uri.parse(url!).toString();
    }
    if (hashtags != null && hashtags.isNotEmpty) {
      String tags = "";
      hashtags.forEach((f) {
        tags += (" " + f.toString() + " ").toString();
      });
      args = <String, dynamic>{
        "captionText": captionText + "\n" + tags.toString(),
        "url": modifiedUrl,
        "trailingText": (trailingText == null || trailingText.isEmpty) ? "" : trailingText
      };
    } else {
      args = <String, dynamic>{
        "captionText": captionText + " ",
        "url": modifiedUrl,
        "trailingText": (trailingText == null || trailingText.isEmpty) ? "" : trailingText
      };
    }
    final String? version = await _channel.invokeMethod('twitterShare', args);
    return version;
  }

  static Future<String?> twitterImageShare(String captionText,
      List<String>? hashtags, String? url, String? trailingText, String path) async {
    Map<String, dynamic> args;
    String modifiedUrl;

    /*File file = File(path);
    Uint8List bytes = file.readAsBytesSync();
    var stickerdata = bytes.buffer.asUint8List();
    final tempDir = await getTemporaryDirectory();
    String stickerAssetName = 'stickerAsset.png';
    final Uint8List stickerAssetAsList = stickerdata;
    final stickerAssetPath = '${tempDir.path}/$stickerAssetName';
    file = await File(stickerAssetPath).create();
    file.writeAsBytesSync(stickerAssetAsList);*/

    if (Platform.isAndroid) {
      modifiedUrl = Uri.parse(url!).toString().replaceAll('#', "");
    } else {
      modifiedUrl = Uri.parse(url!).toString();
    }
    if (hashtags != null && hashtags.isNotEmpty) {
      String tags = "";
      hashtags.forEach((f) {
        tags += ("" + f.toString() + " ").toString();
      });
      args = <String, dynamic>{
        "captionText": captionText + "\n" + tags.toString(),
        "url": modifiedUrl,
        "path": path,
        "trailingText": (trailingText == null || trailingText.isEmpty) ? "" : trailingText
      };
    } else {
      args = <String, dynamic>{
        "captionText": captionText + " ",
        "url": modifiedUrl,
        "trailingText": (trailingText == null || trailingText.isEmpty) ? "" : trailingText
      };
    }
    final String? version = await _channel.invokeMethod('twitterImageShare', args);
    return version;
  }

  static Future<String?> whatsAppShareImage(String captionText,
      List<String>? hashtags, String? url, String? trailingText, String path) async {
    Map<String, dynamic> args;
    String modifiedUrl;

    /*File file = File(path);
    Uint8List bytes = file.readAsBytesSync();
    var stickerdata = bytes.buffer.asUint8List();
    final tempDir = await getTemporaryDirectory();
    String stickerAssetName = 'stickerAsset.png';
    final Uint8List stickerAssetAsList = stickerdata;
    final stickerAssetPath = '${tempDir.path}/$stickerAssetName';
    file = await File(stickerAssetPath).create();
    file.writeAsBytesSync(stickerAssetAsList);*/

    if (Platform.isAndroid) {
      modifiedUrl = Uri.parse(url!).toString().replaceAll('#', "%23");
    } else {
      modifiedUrl = Uri.parse(url!).toString();
    }
    if (hashtags != null && hashtags.isNotEmpty) {
      String tags = "";
      hashtags.forEach((f) {
        tags += ("%23" + f.toString() + " ").toString();
      });
      args = <String, dynamic>{
        "captionText": captionText + "\n" + tags.toString(),
        "url": modifiedUrl,
        "path": path,
        "trailingText": (trailingText == null || trailingText.isEmpty) ? "" : trailingText
      };
    } else {
      args = <String, dynamic>{
        "captionText": captionText + " ",
        "url": modifiedUrl,
        "trailingText": (trailingText == null || trailingText.isEmpty) ? "" : trailingText
      };
    }
    final String? version = await _channel.invokeMethod('whatsAppShareImage', args);
    return version;
  }

  static Future<String?> whatsappShare(String content) async {
    Map<String, dynamic> args = <String, dynamic>{
      "content": content
    };
    final String? version = await _channel.invokeMethod('whatsappShare', args);
    return version;
  }

  static Future<String?> telegramShare(String content) async {
    final Map<String, dynamic> args = <String, dynamic>{
      "content": content
    };
    final String? version = await _channel.invokeMethod('telegramShare', args);
    return version;
  }

  Future<dynamic> readFileAsync(String filePath) async {
    String pathString =  await rootBundle.loadString(filePath);
    print("pathString" + '$pathString');
    return pathString;
  }
}
