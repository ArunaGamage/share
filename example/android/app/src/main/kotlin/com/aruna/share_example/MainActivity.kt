package com.aruna.share_example

import android.os.Bundle
import android.util.Log
import com.aruna.share.SharePlugin
import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        SharePlugin.initialize(this)
        Log.d("Aruna_Plug", " success")
    }
}
