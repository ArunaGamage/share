import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:share/share.dart';

import 'package:image_picker/image_picker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String _facebookShare = 'Unknown';
  String _whatsappShare = 'Unknown';
  String _telegramShare = 'Unknown';
  String _twitterShare = 'Unknown';
  String _instagramStoryShare = 'Unknown';

  final ImagePicker picker = ImagePicker();
  XFile? _imageFile;

  dynamic _pickImageError;
  bool isVideo = false;

  // VideoPlayerController? _controller;
  // VideoPlayerController? _toBeDisposed;
  String? _retrieveDataError;

  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  /*Future<void> selectImage() async {
    XFile? file = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    print("File_Path" + file!.path);
    setState(() {

    });
  }*/

  Future<void> getLostData() async {
    final LostDataResponse response =
    await picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        if (response.type == RetrieveType.video) {
          print(response.file);
        } else {
          print(response.file);
        }
      });
    } else {
      print(response.exception);
    }
  }

  void _onImageButtonPressed(ImageSource source,
      {BuildContext? context, bool isMultiImage = false}) async {
      try {
        final pickedFile = await _picker.pickImage(
          source: source,
          maxWidth: 1000.0,
          maxHeight: 1000.0,
          imageQuality: 100,
        );
        setState(() {
          _imageFile = pickedFile;
        });
      } catch (e) {
        setState(() {
          _pickImageError = e;
        });
      }
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();
    // initFacebookShareLink("https://avatars.githubusercontent.com/u/25635071?v=4");
    // facebookShareQuote("", "https://avatars.githubusercontent.com/u/25635071?v=4");
    // facebookShareImage("", "");
    // whatsappShare("Hello");
    /*twitterShare(
      "This is Social Share twitter example",
      ["hello", "world", "foo", "bar"],
      "https://google.com/#/hello",
      "\nhello",
    );*/
    /*instagramStoryShare("assets/images/art.png",
      "#ffffff",
      "#000000",
      "https://deep-link-urbl",
      "assets/images/art.png",);*/
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    try {
      platformVersion =
          await Share.platformVersion ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future<void> initFacebookShareLink(String link) async {
    String facebookShare;
    try {
      facebookShare =
          await Share.facebookShareLink(link) ?? 'Unknown platform version';
    } on PlatformException {
      facebookShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _facebookShare = facebookShare;
    });
  }

  Future<void> facebookShareQuote(String quote, String link) async {
    String facebookShare;
    try {
      facebookShare =
          await Share.facebookShareQuote(quote, link) ?? 'Unknown platform version';
    } on PlatformException {
      facebookShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _facebookShare = facebookShare;
    });
  }

  Future<void> facebookShareImage(String quote, String path) async {
    String facebookShare;
    try {
      facebookShare =
          await Share.facebookShareImage(quote, path) ?? 'Unknown platform version';
    } on PlatformException {
      facebookShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _facebookShare = facebookShare;
    });
  }

  Future<void> whatsappShare(String content) async {
    String whatsappShare;
    try {
      whatsappShare =
          await Share.whatsappShare(content) ?? 'Unknown platform version';
    } on PlatformException {
      whatsappShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _whatsappShare = whatsappShare;
    });
  }

  Future<void> whatsappImageShare(String captionText,
      List<String>? hashtags, String? url, String? trailingText, String path) async {
    String twitterShare;
    try {
      twitterShare =
          await Share.whatsAppShareImage(
              captionText, hashtags, url, trailingText, path) ?? 'Unknown platform version';
    } on PlatformException {
      twitterShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _twitterShare = twitterShare;
    });
  }

  Future<void> telegramShare(String content) async {
    String telegramShare;
    try {
      telegramShare =
          await Share.telegramShare(content) ?? 'Unknown platform version';
    } on PlatformException {
      telegramShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _telegramShare = telegramShare;
    });
  }

  Future<void> twitterShare(String captionText,
      List<String>? hashtags, String? url, String? trailingText) async {
    String twitterShare;
    try {
      twitterShare =
          await Share.twitterShare(
            captionText, hashtags, url, trailingText,) ?? 'Unknown platform version';
    } on PlatformException {
      twitterShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _twitterShare = twitterShare;
    });
  }

  Future<void> twitterImageShare(String captionText,
      List<String>? hashtags, String? url, String? trailingText, String path) async {
    String twitterShare;
    try {
      twitterShare =
          await Share.twitterImageShare(
            captionText, hashtags, url, trailingText, path) ?? 'Unknown platform version';
    } on PlatformException {
      twitterShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _twitterShare = twitterShare;
    });
  }

  Future<void> instagramStoryShare(String imagePath, String? backgroundTopColor,
    String? backgroundBottomColor, String? attributionURL, String? backgroundImagePath,
  ) async {
    String instagramStoryShare;
    try {
      instagramStoryShare =
          await Share.instagramStoryShare(
              imagePath, backgroundTopColor, backgroundBottomColor, attributionURL, backgroundImagePath) ?? 'Unknown platform version';
    } on PlatformException {
      instagramStoryShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _instagramStoryShare = instagramStoryShare;
    });
  }

  Future<void> instagramShareImage(String quote, String path) async {
    String facebookShare;
    try {
      facebookShare =
          await Share.instagramShareImage(quote, path) ?? 'Unknown platform version';
    } on PlatformException {
      facebookShare = 'Failed to get platform version.';
    }
    if (!mounted) return;
    setState(() {
      _facebookShare = facebookShare;
    });
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
    ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: [
            Center(
              child: Text('Running on: $_platformVersion\n'),
            ),
            SizedBox(height: 60,),
            ElevatedButton(
              style: style,
              onPressed: () {
                isVideo = false;
                _onImageButtonPressed(ImageSource.gallery, context: context);
                // getLostData();
                },
              child: const Text('Select Image'),
            ),
            SizedBox(height: 20,),
            ElevatedButton(
              style: style,
              onPressed: () {
                facebookShareImage("", _imageFile!.path.toString());},
              child: const Text('Facebook Share Image'),
            ),

            SizedBox(height: 20,),
            ElevatedButton(
              style: style,
              onPressed: () {
                instagramShareImage("", _imageFile!.path.toString());
                },
              child: const Text('Instagram Share Image'),
            ),
            SizedBox(height: 20,),
            // Image.file(File(_imageFile!.path)),
            SizedBox(height: 20,),
            ElevatedButton(
              style: style,
              onPressed: () {
                facebookShareQuote("", "https://avatars.githubusercontent.com/u/25635071?v=4");
              },
              child: const Text('Facebook Share Image'),
            ),
            SizedBox(height: 20,),
            ElevatedButton(
              style: style,
              onPressed: () {
                twitterImageShare(
                  "This is Social Share twitter example",
                  ["hello", "world", "foo", "bar"],
                  "https://google.com/#/hello",
                  "\nhello",
                  _imageFile!.path.toString(),
                );
              },
              child: const Text('Twitter Share Image'),
            ),
            SizedBox(height: 20,),
            ElevatedButton(
              style: style,
              onPressed: () {
                whatsappImageShare(
                  "This is Social Share twitter example",
                  ["hello", "world", "foo", "bar"],
                  "https://google.com/#/hello",
                  "\nhello",
                  _imageFile!.path.toString(),
                );
              },
              child: const Text('WhatsApp Share Image'),
            ),
          ],
        ),
      ),
    );
  }
}

typedef void OnPickImageCallback(
    double? maxWidth, double? maxHeight, int? quality);
