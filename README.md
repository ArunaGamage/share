# share

A new Flutter project.

## Getting Started

****************************************************************************************
Open Android Activity and paste below code.

	override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            SharePlugin.initialize(this)
            Log.d("Aruna_Plug", " success")
    	}

All the social share colling method example can see in 'example.main.dart' file.

****************************************************************************************


This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

