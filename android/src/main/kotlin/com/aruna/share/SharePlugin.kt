package com.aruna.share

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import androidx.annotation.NonNull
import androidx.core.content.FileProvider
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.io.File


/** SharePlugin */
class SharePlugin: FlutterPlugin, MethodCallHandler {
  private val TAG = this::class.java.simpleName
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var binding: FlutterPlugin.FlutterPluginBinding
  private var utils: ShareUtils =
    ShareUtils()

//  private val callbackManager: CallbackManager? = null
//  var shareDialog: ShareDialog? = null

  companion object {
    private  lateinit var activitys: Activity
    @JvmStatic
    fun initialize(activity: Activity) {
      activitys = activity
    }
  }

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "share")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else if (call.method == "smsShare") {
      smsShare(call, result)
    } else if (call.method == "facebookStoryShare") {
//      facebookStoryShare(call, result)
    } else if (call.method == "instagramStoryShare") {
      instagramStoryShare(call, result)
    } else if (call.method == "instagramShareImage") {
      instagramShareImage(call, result)
    } else if (call.method == "facebookShareLink") {
//      facebookShareLink(call, result)
    } else if (call.method == "facebookShareQuote") {
//      facebookShareQuote(call, result)
    } else if (call.method == "facebookShareImage") {
      facebookShareImage(call, result)
//      facebookStoryShare(call, result)
    } else if (call.method == "whatsappShare") {
      whatsappShare(call, result)
    } else if (call.method == "whatsAppShareImage") {
      whatsAppShareImage(call, result)
    } else if (call.method == "telegramShare") {
      telegramShare(call, result)
    } else if (call.method == "twitterShare") {
      twitterShare(call, result)
    } else if (call.method == "twitterImageShare") {
      twitterImageShare(call, result)
    } else {
      result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
    this.binding = binding
//    FacebookSdk.setApplicationId("fb4239416086165163")
//    FacebookSdk.sdkInitialize(binding.applicationContext)
//    FacebookSdk.publishInstallAsync(binding.applicationContext, "4239416086165163")

//    FacebookSdk.setApplicationId("fb4239416086165163")
//    FacebookSdk.sdkInitialize(binding.applicationContext)
//    Log.e("Facebook_Initialized", " " +FacebookSdk.isInitialized())
  }

  private fun smsShare(@NonNull call: MethodCall, @NonNull result: Result) {
    val content: String? = call.argument("message")
    val intent = Intent(Intent.ACTION_SENDTO)
    intent.addCategory(Intent.CATEGORY_DEFAULT)
    intent.type = "vnd.android-dir/mms-sms"
    intent.data = Uri.parse("sms:" )
    intent.putExtra("sms_body", content)
    try {
      binding.applicationContext.startActivity(intent)
      result.success("true")
    } catch (ex: ActivityNotFoundException) {
      result.success("false")
    }
  }

  private fun facebookStoryShare(@NonNull call: MethodCall, @NonNull result: Result) {
    val text: String? = call.argument("text")
    val path: String? = call.argument("path")

//    val stickerImage: String? = call.argument("stickerImage")
//    val backgroundTopColor: String? = call.argument("backgroundTopColor")
//    val backgroundBottomColor: String? = call.argument("backgroundBottomColor")
//    val attributionURL: String? = call.argument("attributionURL")
//    val appId: String? = call.argument("appId")
    val appId: String? = "4239416086165163"

    val file =  File(path)
    var bitmap:Bitmap? = decodeImage(path)
    val stickerImageFile = utils.getImageUriFromBitmap(activitys, bitmap)
    val intent = Intent("com.facebook.stories.ADD_TO_STORY")
    intent.type = "image/*"
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    intent.putExtra("com.facebook.platform.extra.APPLICATION_ID", appId)
    intent.putExtra("interactive_asset_uri", stickerImageFile)
//    intent.putExtra("content_url", attributionURL)
//    intent.putExtra("top_background_color", backgroundTopColor)
//    intent.putExtra("bottom_background_color", backgroundBottomColor)
    Log.d("", activitys.toString())
    // Instantiate activity and verify it will resolve implicit intent
    val activity: Activity = activitys as Activity
    activity.grantUriPermission("com.facebook.katana", stickerImageFile, Intent.FLAG_GRANT_READ_URI_PERMISSION)
    activitys.startActivity(intent)
    result.success("success")
  }

  /*private fun facebookShareLink(@NonNull call: MethodCall, @NonNull result: Result) {
    Log.e("Facebook_Initialized_S", " " +FacebookSdk.isInitialized())
    val link: String? = call.argument("link")
    *//*val backgroundTopColor: String? = call.argument("backgroundTopColor")
    val backgroundBottomColor: String? = call.argument("backgroundBottomColor")
    val attributionURL: String? = call.argument("attributionURL")*//*

    var callbackManager = CallbackManager.Factory.create()
    val shareDialog = ShareDialog(activitys)
    shareDialog.registerCallback(callbackManager, object : FacebookCallback<Sharer.Result?> {
      override fun onSuccess(result: Sharer.Result?) {
        Log.d(TAG, "success")
      }

      override fun onError(error: FacebookException) {
        Log.d(TAG, "error")
      }

      override fun onCancel() {
        Log.d(TAG, "cancel")
      }
    })
    if (ShareDialog.canShow(ShareLinkContent::class.java)) {
      val linkContent = ShareLinkContent.Builder()
        .setContentTitle("Game Result Highscore")
        .setContentDescription("My new highscore is " + "!!")
        .setContentUrl(Uri.parse(link)) //.setImageUrl(Uri.parse("android.resource://de.ginkoboy.flashcards/" + R.drawable.logo_flashcards_pro))
//        .setImageUrl(Uri.parse("https://avatars.githubusercontent.com/u/25635071?v=4"))
        .build()
      shareDialog.show(linkContent)
    }
  }*/

  /*private fun facebookShareQuote(@NonNull call: MethodCall, @NonNull result: Result) {
    Log.e("Facebook_Initialized_S", " " +FacebookSdk.isInitialized())
    val text: String? = call.argument("text")
    val link: String? = call.argument("link")

    var callbackManager = CallbackManager.Factory.create()
    val shareDialog = ShareDialog(activitys)
    shareDialog.registerCallback(callbackManager, object : FacebookCallback<Sharer.Result?> {
      override fun onSuccess(result: Sharer.Result?) {
        Log.d(TAG, "success")
      }

      override fun onError(error: FacebookException) {
        Log.d(TAG, "error")
      }

      override fun onCancel() {
        Log.d(TAG, "cancel")
      }
    })
    if (ShareDialog.canShow(ShareLinkContent::class.java)) {
      val content = ShareLinkContent.Builder()
        .setContentUrl(Uri.parse(link))
        .setQuote(text)
        .build()
      shareDialog.show(content)
    }

  }*/

  private fun facebookShareImage(@NonNull call: MethodCall, @NonNull result: Result) {
    val text: String? = call.argument("text")
    val path: String? = call.argument("path")

    try {
      val facebook = Intent(Intent.ACTION_SEND)
      facebook.putExtra(Intent.EXTRA_TEXT, text)
      var bitmaps: Bitmap = BitmapFactory.decodeFile(path)
      facebook.putExtra(Intent.EXTRA_STREAM, utils.getImageUriFromBitmap(activitys,bitmaps))
      facebook.type = "image/jpeg"
      facebook.`package` = "com.facebook.katana"
      val pm: PackageManager = activitys.getPackageManager()
      val lract = pm.queryIntentActivities(facebook, PackageManager.MATCH_DEFAULT_ONLY)
      var resolved = false
      for (ri in lract) {
        if (true) {
          facebook.setClassName(
            ri.activityInfo.packageName,
            ri.activityInfo.name
          )
          resolved = true
          break
        }
      }
      activitys.startActivity(facebook)
    } catch (e: ActivityNotFoundException) {

    }
  }

  private fun instagramShareImage(@NonNull call: MethodCall, @NonNull result: Result) {
    val text: String? = call.argument("text")
    val path: String? = call.argument("path")
    val writePath: String? = call.argument("writePath")

    try {
      val instagram = Intent(Intent.ACTION_SEND)
      instagram.putExtra(Intent.EXTRA_TEXT, text)
      var bitmaps: Bitmap = BitmapFactory.decodeFile(path)
      instagram.putExtra(Intent.EXTRA_STREAM, utils.getImageUriFromBitmap(activitys,bitmaps))
      instagram.type = "image/jpeg"
      instagram.`package` = "com.instagram.android"
      val pm: PackageManager = activitys.getPackageManager()
      val lract = pm.queryIntentActivities(instagram, PackageManager.MATCH_DEFAULT_ONLY)
      var resolved = false
      /*for (ri in lract) {
        if (true) {
          instagram.setClassName(
            ri.activityInfo.packageName,
            ri.activityInfo.name
          )
          resolved = true
          break
        }
      }*/
      activitys.startActivity(instagram)
    } catch (e: ActivityNotFoundException) {

    }
  }

  private fun instagramStoryShare(@NonNull call: MethodCall, @NonNull result: Result) {
    val stickerImage: String? = call.argument("stickerImage")
    val backgroundImage: String? = call.argument("backgroundImage")

    val backgroundTopColor: String? = call.argument("backgroundTopColor")
    val backgroundBottomColor: String? = call.argument("backgroundBottomColor")
    val attributionURL: String? = call.argument("attributionURL")
    val file =  File(binding.applicationContext.cacheDir,stickerImage)
    val stickerImageFile = FileProvider.getUriForFile(binding.applicationContext, binding.applicationContext.packageName + ".com.shekarmudaliyar.social_share", file)

    val intent = Intent("com.instagram.share.ADD_TO_STORY")
    intent.type = "image/*"
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    intent.putExtra("interactive_asset_uri", stickerImageFile)
    if (backgroundImage!=null) {
      //check if background image is also provided
      val backfile =  File(binding.applicationContext.cacheDir,backgroundImage)
      val backgroundImageFile = FileProvider.getUriForFile(binding.applicationContext, binding.applicationContext.packageName + ".com.shekarmudaliyar.social_share", backfile)
      intent.setDataAndType(backgroundImageFile,"image/*")
    }

    intent.putExtra("content_url", attributionURL)
    intent.putExtra("top_background_color", backgroundTopColor)
    intent.putExtra("bottom_background_color", backgroundBottomColor)
    Log.d("", binding.applicationContext.toString())
    // Instantiate activity and verify it will resolve implicit intent
    val activity: Activity = binding.applicationContext as Activity
    activity.grantUriPermission("com.instagram.android", stickerImageFile, Intent.FLAG_GRANT_READ_URI_PERMISSION)
    if (activity.packageManager.resolveActivity(intent, 0) != null) {
      binding.applicationContext.startActivity(intent)
      result.success("success")
    } else {
      result.success("error")
    }
  }

  private fun whatsappShare(@NonNull call: MethodCall, @NonNull result: Result) {
    val content: String? = call.argument("content")
    val whatsappIntent = Intent(Intent.ACTION_SEND)
    whatsappIntent.type = "text/plain"
    whatsappIntent.setPackage("com.whatsapp")
    whatsappIntent.putExtra(Intent.EXTRA_TEXT, content)
    try {
      activitys.startActivity(whatsappIntent)
      result.success("true")
    } catch (ex: ActivityNotFoundException) {
      result.success("false")
    }
  }

  private fun whatsAppShareImage(@NonNull call: MethodCall, @NonNull result: Result) {
    val text: String? = call.argument("text")
    val path: String? = call.argument("path")

    try {
      val instagram = Intent(Intent.ACTION_SEND)
      instagram.putExtra(Intent.EXTRA_TEXT, text)
      var bitmaps: Bitmap = BitmapFactory.decodeFile(path)
      instagram.putExtra(Intent.EXTRA_STREAM, utils.getImageUriFromBitmap(activitys,bitmaps))
      instagram.type = "image/jpeg"
      instagram.`package` = "com.whatsapp"
      val pm: PackageManager = activitys.getPackageManager()
      val lract = pm.queryIntentActivities(instagram, PackageManager.MATCH_DEFAULT_ONLY)
      var resolved = false
      for (ri in lract) {
        if (true) {
          instagram.setClassName(
            ri.activityInfo.packageName,
            ri.activityInfo.name
          )
          resolved = true
          break
        }
      }
      activitys.startActivity(instagram)
    } catch (e: ActivityNotFoundException) {

    }
  }

  private fun telegramShare(@NonNull call: MethodCall, @NonNull result: Result) {
    val content: String? = call.argument("content")
    val telegramIntent = Intent(Intent.ACTION_SEND)
    telegramIntent.type = "text/plain"
    telegramIntent.setPackage("org.telegram.messenger")
    telegramIntent.putExtra(Intent.EXTRA_TEXT, content)
    try {
      activitys.startActivity(telegramIntent)
      result.success("true")
    } catch (ex: ActivityNotFoundException) {
      result.success("false")
    }
  }

  private fun twitterShare(@NonNull call: MethodCall, @NonNull result: Result) {
    val text: String? = call.argument("captionText")
    val url: String? = call.argument("url")
    val trailingText: String? = call.argument("trailingText")
    val urlScheme = "http://www.twitter.com/intent/tweet?text=$text$url$trailingText"
    Log.d("log",urlScheme)
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse(urlScheme)
    try {
      activitys.startActivity(intent)
      result.success("true")
    } catch (ex: ActivityNotFoundException) {
      result.success("false")
    }
  }

  private fun twitterImageShare(@NonNull call: MethodCall, @NonNull result: Result) {
    val text: String? = call.argument("captionText")
    val url: String? = call.argument("url")
    val trailingText: String? = call.argument("trailingText")
    val urlScheme = "http://www.twitter.com/intent/tweet?text=$text$url$trailingText"
    val filename: String? = call.argument("path")
    val imageFile = File(filename)

//    Log.d("log",urlScheme)
    /*val intent = Intent(Intent.ACTION_VIEW)
    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile))
    intent.data = Uri.parse(urlScheme)
    try {
      activitys.startActivity(intent)
      result.success("true")
    } catch (ex: ActivityNotFoundException) {
      result.success("false")
    }*/

    try {
      val tweetIntent = Intent(Intent.ACTION_SEND)
      tweetIntent.putExtra(Intent.EXTRA_TEXT, text)
      var bitmaps: Bitmap = BitmapFactory.decodeFile(filename)
      tweetIntent.putExtra(Intent.EXTRA_STREAM, utils.getImageUriFromBitmap(activitys,bitmaps))
      tweetIntent.type = "image/jpeg"
      tweetIntent.`package` = "com.twitter.android"
      val pm: PackageManager = activitys.getPackageManager()
      val lract = pm.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY)
      var resolved = false
      for (ri in lract) {
        if (ri.activityInfo.name.contains("twitter")) {
          tweetIntent.setClassName(
            ri.activityInfo.packageName,
            ri.activityInfo.name
          )
          resolved = true
          break
        }
      }
      activitys.startActivity(tweetIntent)
    } catch (e: ActivityNotFoundException) {

    }
  }

  fun checkInstalledApps() {
    //check if the apps exists
    //creating a mutable map of apps
    var apps:MutableMap<String, Boolean> = mutableMapOf()
    //assigning package manager
    val pm: PackageManager = activitys.packageManager
    //get a list of installed apps.
    val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)
    //intent to check sms app exists
    val intent = Intent(Intent.ACTION_SENDTO).addCategory(Intent.CATEGORY_DEFAULT)
    intent.type = "vnd.android-dir/mms-sms"
    intent.data = Uri.parse("sms:" )
    val resolvedActivities: List<ResolveInfo>  = pm.queryIntentActivities(intent, 0)
    //if sms app exists
    apps["sms"] = resolvedActivities.isNotEmpty()
    //if other app exists
    apps["instagram"] = packages.any  { it.packageName.toString().contentEquals("com.instagram.android") }
    apps["facebook"] = packages.any  { it.packageName.toString().contentEquals("com.facebook.katana") }
    apps["twitter"] = packages.any  { it.packageName.toString().contentEquals("com.twitter.android") }
    apps["whatsapp"] = packages.any  { it.packageName.toString().contentEquals("com.whatsapp") }
    apps["telegram"] = packages.any  { it.packageName.toString().contentEquals("org.telegram.messenger") }

  }

  fun decodeImage(path: String?): Bitmap? {
    val options: BitmapFactory.Options
    try {
      var bitmap = BitmapFactory.decodeFile(path)
      bitmap = Bitmap.createScaledBitmap(bitmap,bitmap.width,bitmap.height,true)
      return bitmap
    } catch (e: OutOfMemoryError) {
      try {
        options = BitmapFactory.Options()
        options.inSampleSize = 2
        var bitmap = BitmapFactory.decodeFile(path, options)
        bitmap = Bitmap.createScaledBitmap(bitmap,bitmap.width,bitmap.height,true)
        return bitmap
      } catch (excepetion: Exception) {
        Log.e("", excepetion.message!!)
      }
    }
    return null
  }
}
